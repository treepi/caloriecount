//
//  CoreDataStack.swift
//  CalorieCount
//
//  Created by k ely on 3/13/15.
//  Copyright (c) 2015 flyingfresh. All rights reserved.
//

import Foundation
import CoreData

let kDataModel = "CalorieCount"
let kStoreName = "CalorieCount.sqlite"
let options = [
  NSMigratePersistentStoresAutomaticallyOption: true,
  NSInferMappingModelAutomaticallyOption: true]

class CoreDataStack {
  
  lazy var applicationDocumentsDirectory: NSURL = {
    let urls = NSFileManager.defaultManager().URLsForDirectory(.DocumentDirectory, inDomains: .UserDomainMask)
    return urls[urls.count-1] as NSURL
    }()
  
  lazy var managedObjectModel: NSManagedObjectModel = {
    let modelURL = NSBundle.mainBundle().URLForResource(kDataModel, withExtension: "momd")!
    return NSManagedObjectModel(contentsOfURL: modelURL)!
    }()
  
  lazy var persistentStoreCoordinator: NSPersistentStoreCoordinator? = {
    var coordinator: NSPersistentStoreCoordinator? = NSPersistentStoreCoordinator(managedObjectModel: self.managedObjectModel)
    let url = self.applicationDocumentsDirectory.URLByAppendingPathComponent(kStoreName)
    var error: NSError? = nil
    var failureReason = "There was an error creating or loading the application's saved data."
    if coordinator!.addPersistentStoreWithType(NSSQLiteStoreType, configuration: nil,
      URL: url,
      options: options,
      error: &error) == nil {
        coordinator = nil
        println("Error Could Not Add Persistent Store")
        abort()
    }
    return coordinator
    }()
  
  lazy var managedObjectContext: NSManagedObjectContext? = {
    if let coordinator = self.persistentStoreCoordinator {
      var managedObjectContext = NSManagedObjectContext()
      managedObjectContext.persistentStoreCoordinator = coordinator
      return managedObjectContext
    } else { return nil }
    }()
  
  // MARK: - Core Data Saving support
  
  func saveContext () {
    if let moc = self.managedObjectContext {
      var error: NSError? = nil
      if moc.hasChanges && !moc.save(&error) {
        NSLog("Unresolved error \(error), \(error!.userInfo)")
        abort()
      }
    }
  }
}