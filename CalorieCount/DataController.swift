//
//  DataController.swift
//  CalorieCount
//
//  Created by k ely on 3/12/15.
//  Copyright (c) 2015 flyingfresh. All rights reserved.
//

import Foundation
import UIKit
import CoreData

class DataController {
  class func jsonAsUSDAIdAndNameSearchResults(json: NSDictionary) -> [(name: String, idValue: String)] {
    var usdaItemsSearchResults: [(name: String, idValue: String)] = []
    var searchResult: (name: String, idValue: String)
    if let results = json["hits"] as? [AnyObject] {
      for itemDictionary in results {
        if let idValue = itemDictionary["_id"] as? String{
          if let fieldsDictionary = itemDictionary["fields"] as? NSDictionary {
            if let name = fieldsDictionary["item_name"] as? String {
              searchResult = (name: name, idValue: idValue)
              usdaItemsSearchResults += [searchResult]
            }
          }
        }
      }
    }
    return usdaItemsSearchResults
  }
  
  func saveUSDAItemsForId(idValue: String, json: NSDictionary) {
    if let results = json["hits"] as? [AnyObject] {
      for itemDictionary in results {
        if itemDictionary["_id"] != nil && itemDictionary["_id"] as String == idValue {
          let coreDataStack = (UIApplication.sharedApplication().delegate as AppDelegate).coreDataStack
          var requestForUSDAItem = NSFetchRequest(entityName: "USDAItem")
          let itemDictionaryId = itemDictionary["_id"]! as String
          let predicate = NSPredicate(format: "idValue == %@", itemDictionaryId)
          requestForUSDAItem.predicate = predicate
          var error: NSError?
          var items = coreDataStack.managedObjectContext?.executeFetchRequest(requestForUSDAItem, error: &error)
//          var count = coreDataController.managedObjectContext.countForFetchRequest(requestForUSDAItem, error: &error)
          if items?.count != 0 {
            println("The item was already saved")
            return
          }
          else {
            println("Lets Save this to CoreData!")
            let entityDescription = NSEntityDescription.entityForName("USDAItem", inManagedObjectContext: coreDataStack.managedObjectContext!)
            let usdaItem = USDAItem(entity: entityDescription!, insertIntoManagedObjectContext: coreDataStack.managedObjectContext!)
            usdaItem.idValue = itemDictionary["_id"] as String
            usdaItem.dateAdded = NSDate()
            if let fieldsDictionary = itemDictionary["fields"] as? NSDictionary {
              if let name = fieldsDictionary["item_name"] as? String {
                usdaItem.name = name as String
                if let usdaFieldsDictionary = fieldsDictionary["usda_fields"] as? NSDictionary {
                  if let calciumDictionary = usdaFieldsDictionary["CA"] as? NSDictionary {
                    let calciumValue: AnyObject = calciumDictionary["value"]! as AnyObject
                    usdaItem.calsium = "\(calciumValue)"
                  } else {
                    usdaItem.calsium = "0"
                  }
                  if let carbohydrateDictionary = usdaFieldsDictionary["CHOCDF"] as? NSDictionary {
                    let carbohydrateValue: AnyObject = carbohydrateDictionary["value"]! as AnyObject
                    usdaItem.carbohydrate = "\(carbohydrateValue)"
                  } else {
                    usdaItem.carbohydrate = "0"
                  }
                  if let fatTotalDictionary = usdaFieldsDictionary["FAT"] as? NSDictionary {
                    let fatTotalValue: AnyObject = fatTotalDictionary["value"]! as AnyObject
                    usdaItem.fatTotal = "\(fatTotalValue)"
                  } else {
                    usdaItem.fatTotal = "0"
                  }
                  if let cholesterolDictionary = usdaFieldsDictionary["CHOLE"] as? NSDictionary {
                    let cholesterolValue: AnyObject = cholesterolDictionary["value"]! as AnyObject
                    usdaItem.cholesterol = "\(cholesterolValue)"
                  } else {
                    usdaItem.cholesterol = "0"
                  }
                  if let proteinDictionary = usdaFieldsDictionary["PROCNT"] as? NSDictionary {
                    let proteinValue: AnyObject = proteinDictionary["value"]! as AnyObject
                    usdaItem.protein = "\(proteinValue)"
                  } else {
                    usdaItem.protein = "0"
                  }
                  if let sugarDictionary = usdaFieldsDictionary["SUGAR"] as? NSDictionary {
                    let sugarValue: AnyObject = sugarDictionary["value"]! as AnyObject
                    usdaItem.sugar = "\(sugarValue)"
                  } else {
                    usdaItem.sugar = "0"
                  }
                  if let vitaminCDictionary = usdaFieldsDictionary["VITC"] as? NSDictionary {
                    let vitaminCValue: AnyObject = vitaminCDictionary["value"]! as AnyObject
                    usdaItem.vitaminC = "\(vitaminCValue)"
                  } else {
                    usdaItem.vitaminC = "0"
                  }
                  if let energyDictionary = usdaFieldsDictionary["ENERC_KCAL"] as? NSDictionary {
                    let energyValue: AnyObject = energyDictionary["value"]! as AnyObject
                    usdaItem.protein = "\(energyValue)"
                  } else {
                    usdaItem.protein = "0"
                  }
                  coreDataStack.saveContext()
                }
              }
            }
          }
        }
      }
    }
  }
}