//
//  ViewController.swift
//  CalorieCount
//
//  Created by k ely on 3/11/15.
//  Copyright (c) 2015 flyingfresh. All rights reserved.
//

import UIKit
import CoreData

struct Data {
  static var foods = [
    "banana", "apple", "lemon", "pizza", "garden salad",
    "chicken parma", "steak", "orange juice", "chocolate",
    "mango", "pasta", "egg", "cheese", "honey", "blueberry"]
  static var scopeButtonTitles = [
    "Recommended",
    "Search Results",
    "Saved"]
  static var kAppId = "637cdce4"
  static var kAppKey = "59ea7996d5fbbd6d1f92f9a0ae3f099c"
}

class ViewController: UIViewController {
  
  var foods = [String]()
  var filteredFoods = [String]()
  var jsonResponse: NSDictionary!
  var apiSearchFoods: [(name: String, idValue: String)] = []
  var searchController: UISearchController!
  var scopeButtonTitles = Data.scopeButtonTitles
  var coreDataStack: CoreDataStack!
  var dataController = DataController()
  @IBOutlet weak var tableView: UITableView!
  
  override func viewDidLoad() {
    super.viewDidLoad()
    foods = Data.foods
    searchController = UISearchController(searchResultsController: nil)
    searchController.searchResultsUpdater = self
    searchController.searchBar.scopeButtonTitles = scopeButtonTitles
    searchController.hidesNavigationBarDuringPresentation = false
    searchController.hidesNavigationBarDuringPresentation = false
    searchController.searchBar.delegate = self
    searchController.searchBar.frame = CGRect(
      x: searchController.searchBar.frame.origin.x,
      y: searchController.searchBar.frame.origin.y,
      width: searchController.searchBar.frame.size.width,
      height: 44)
    tableView.tableHeaderView = searchController.searchBar
    self.definesPresentationContext = true
  }
  
  override func didReceiveMemoryWarning() {
    super.didReceiveMemoryWarning()
  }
  
  // Helper
  
  func filterContentForSearch(searchText: String, scope: Int) {
    filteredFoods = foods.filter({ (food: String) -> Bool in
      var foodMatch = food.rangeOfString(searchText)
      return foodMatch != nil
    })
  }
  
  func getRequest(searchString : String) {
    let url = NSURL(string: "https://api.nutritionix.com/v1_1/search/\(searchString)?results=0%3A20&cal_min=0&cal_max=50000&fields=item_name%2Cbrand_name%2Citem_id%2Cbrand_id&appId=\(Data.kAppId)&appKey=\(Data.kAppKey)")
    let session = NSURLSession.sharedSession()
    let task = session.dataTaskWithURL(url!, completionHandler: { (data, response, error) -> Void in
      var dataString = NSString(data: data, encoding: NSUTF8StringEncoding)
      println(data)
      println(response)
    })
    task.resume()
  }
  
  func makeRequest(searchString : String) {
    let url = NSURL(string: "https://api.nutritionix.com/v1_1/search/")
    var request = NSMutableURLRequest(URL: url!)
    let session = NSURLSession.sharedSession()
    request.HTTPMethod = "POST"
    var params = [
      "appId" : Data.kAppId,
      "appKey" : Data.kAppKey,
      "fields" : ["item_name", "brand_name", "keywords", "usda_fields"],
      "limit" : "50",
      "query" : searchString,
      "filters" : ["exists":["usda_fields": true]]]
    request.addValue("application/json", forHTTPHeaderField: "Content-Type")
    request.addValue("application/json", forHTTPHeaderField: "Accept")
    var error: NSError?
    request.HTTPBody = NSJSONSerialization.dataWithJSONObject(params, options: nil, error: &error)
    var task = session.dataTaskWithRequest(request, completionHandler: { (data, response, err) -> Void in
//      var stringData = NSString(data: data, encoding: NSUTF8StringEncoding)
//      println(stringData)
      var conversionError: NSError?
      var jsonDictionary = NSJSONSerialization.JSONObjectWithData(data, options: NSJSONReadingOptions.MutableLeaves, error: &conversionError) as? NSDictionary
//      println(jsonDictionary)
      if let conversionError = conversionError {
        println(conversionError.localizedDescription)
        let errorString = NSString(data: data, encoding: NSUTF8StringEncoding)
        println("Error in Parsing  \(errorString)")
      } else {
        if let jsonDictionary = jsonDictionary {
          self.jsonResponse = jsonDictionary
          self.apiSearchFoods = DataController.jsonAsUSDAIdAndNameSearchResults(jsonDictionary)
          dispatch_async(dispatch_get_main_queue(), { () -> Void in
            self.tableView.reloadData()
          })
        } else {
          let errorString = NSString(data: data, encoding: NSUTF8StringEncoding)
          println("Error Could Not Parse Json \(errorString)")
        }
      }
    })
    task.resume()
  }
}

// MARK: - UISearchResultsUpdating

extension ViewController: UISearchResultsUpdating {
  func updateSearchResultsForSearchController(searchController: UISearchController) {
    let searchString = searchController.searchBar.text
    let selectedScopeButtonIndex = searchController.searchBar.selectedScopeButtonIndex
    filterContentForSearch(searchString, scope: selectedScopeButtonIndex)
    tableView.reloadData()
  }
}

// MARK: - UITableViewDataSource

extension ViewController: UITableViewDataSource {
  func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
    let selectedScopeButtonIndex = self.searchController.searchBar.selectedScopeButtonIndex
    if selectedScopeButtonIndex == 0 {
      if searchController.active {
        return filteredFoods.count
      } else {
        return foods.count
      }
    } else if selectedScopeButtonIndex == 1 {
        return apiSearchFoods.count
      } else {
        return 0
      }
  }

  func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
    let cell = tableView.dequeueReusableCellWithIdentifier("Food Cell") as FoodCell
    let selectedScopeButtonIndex = self.searchController.searchBar.selectedScopeButtonIndex
    var foodName: String
    if selectedScopeButtonIndex == 0 {
      foodName = {
        return self.searchController.active ?
          self.filteredFoods[indexPath.row] :
          self.foods[indexPath.row]
        }()
    } else if selectedScopeButtonIndex == 1 {
        foodName = apiSearchFoods[indexPath.row].name
    } else {
      foodName = ""
    }
    cell.nameLabel.text = foodName
    cell.accessoryType = UITableViewCellAccessoryType.DisclosureIndicator
    return cell
  }
}

// MARK: - UITableViewDelegate

extension ViewController: UITableViewDelegate {
  func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
    let selectedScopeButtonIndex = self.searchController.searchBar.selectedScopeButtonIndex
    var searchfoodName: String
    if selectedScopeButtonIndex == 0 {
      searchfoodName = {
        return self.searchController.active ?
          self.filteredFoods[indexPath.row] :
          self.foods[indexPath.row]
        }()
      self.searchController.searchBar.selectedScopeButtonIndex = 1
      makeRequest(searchfoodName)

    } else if selectedScopeButtonIndex == 1 {
      let idValue = apiSearchFoods[indexPath.row].idValue
      dataController.saveUSDAItemsForId(idValue, json: jsonResponse)
    } else {
      
    }
  }
}

// MARK: - UISearchBarDelegate

extension ViewController: UISearchBarDelegate {
  func searchBarSearchButtonClicked(searchBar: UISearchBar) {
    self.searchController.searchBar.selectedScopeButtonIndex = 1
    makeRequest(searchBar.text)
    //    getRequest(searchBar.text)
  }
  
  func searchBar(searchBar: UISearchBar, selectedScopeButtonIndexDidChange selectedScope: Int) {
    let range = NSMakeRange(0, 1)
    let sections = NSIndexSet(indexesInRange: range)
    tableView.reloadSections(sections, withRowAnimation: UITableViewRowAnimation.Automatic)
  }
}

//var categoryMatch = (scope == "All") || (exercise.category == scope)
//var stringMatch = exercise.name.rangeOfString(searchText)
//return categoryMatch && (stringMatch != nil)
//
//func searchDisplayController(controller: UISearchDisplayController!, shouldReloadTableForSearchString searchString: String!) -> Bool {
//  let scopes = self.searchDisplayController!.searchBar.scopeButtonTitles as [String]
//  let selectedScope = scopes[self.searchDisplayController!.searchBar.selectedScopeButtonIndex] as String
//  self.filterContentForSearchText(searchString, scope: selectedScope)
//  return true
//}
//
//func searchDisplayController(controller: UISearchDisplayController!,
//  shouldReloadTableForSearchScope searchOption: Int) -> Bool {
//    let scope = self.searchDisplayController!.searchBar.scopeButtonTitles as [String]
//    self.filterContentForSearchText(self.searchDisplayController!.searchBar.text, scope: scope[searchOption])
//    return true
//}
//
//func searchDisplayController(controller: UISearchDisplayController, didLoadSearchResultsTableView tableView: UITableView) {
//  self.searchDisplayController!.searchResultsTableView.rowHeight = 44
//}

